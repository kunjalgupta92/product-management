<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();
Route::group(['middleware' => ['web','auth'] ], function () {
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::get('category', [CategoryController::class, 'index'])->name('category.index');
    Route::post('categorycreate', [CategoryController::class, 'store'])->name('category.store');
    Route::get('subcategory', [SubCategoryController::class, 'index'])->name('subcategory.index');
    Route::post('subcategorycreate', [SubCategoryController::class, 'store'])->name('subcategory.store');
    Route::get('product', [ProductController::class, 'index'])->name('products.index');
    Route::post('productcreate', [ProductController::class, 'store'])->name('products.store');
});
