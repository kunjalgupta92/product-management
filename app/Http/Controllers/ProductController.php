<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\ProductsVarient;
use App\Models\SubCategory;
use App\Models\Product;
use Illuminate\Http\Request;
use Exception;
use Validator;
use Response;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
         $categories = Categories::all();
         $subcategories=SubCategory::join('categories', 'categories.id', '=', 'sub_categories.category_id')->get(['sub_categories.*', 'categories.name AS categoryName']);
         return view('products.list',compact('subcategories', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subcategoryID=$request->subcategoryid;

        if($request->productcolor!=null) {
            $product = new Product();
            $product->title = $request->productname;
            $product->brand = $request->productbrand;
            $product->subcategory_id = $subcategoryID;
            $product->save();

            foreach ($request->productcolor as $key => $value) {
                $imageName = time() . '.' . $request->productimg[$key]->extension();
                $request->productimg[$key]->move(public_path('images'), $imageName);

                $productVarient = new ProductsVarient();
                $productVarient->qty = $request->productqty[$key];
                $productVarient->price = $request->productprice[$key];
                $productVarient->color = $request->productcolor[$key];
                $productVarient->size = $request->productsize[$key];
                $productVarient->description = $request->productdesc[$key];
                $productVarient->image = $imageName;
                $productVarient->product_id = $product->id;
                $productVarient->save();
            }
        }
        else{
            throw new Exception("Please add product varient", 30);
        }
        return redirect()->route('products.index')
                        ->with('message','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Int $id)
    {
        $product = ProductsVarient::join('products', 'products.id', '=', 'products_varient.product_id')
            ->join('sub_categories', 'products.subcategory_id', '=', 'sub_categories.id')
            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
            ->select('products.*', 'products_varient.*', 'sub_categories.name as sub_category_name', 'categories.name as category_name')
            ->where('products_varient.id',$id)->get();
        return $product;
    }

    public function list(Request $request)
    {
        $products = Product::join('products_varient', 'products.id', '=', 'products_varient.product_id')
            ->join('sub_categories', 'products.subcategory_id', '=', 'sub_categories.id')
            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
            ->select('products.*', 'products_varient.*', 'sub_categories.name as sub_category_name', 'categories.name as category_name');

        if ($request->title)
        {
            $products = $products->where('products.title',  'like', '%' . $request->title . '%')->orderBy("products_varient.price", 'ASC');;
        }

        if ($request->subCategory)
        {
            $products = $products->whereIn('products.subcategory_id', $request->subCategory);
        }

        if ($request->brand)
        {
            $products = $products->where('products.brand', 'like', '%' . $request->brand . '%');
        }

        if ($request->category)
        {
            $products = $products->whereIn('sub_categories.category_id', $request->category);
        }

        if ($request->priceFrom && $request->priceTo)
        {
            $products = $products->whereBetween('products_varient.price', [$request->priceFrom, $request->priceTo]);
        }

        if($request->sort) {
            $products = $products->orderBy("products_varient.price", $request->sort);
        }

        return $products->get();
    }

}
