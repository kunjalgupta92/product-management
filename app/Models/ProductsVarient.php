<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsVarient extends Model
{
    protected $table = 'products_varient';

    protected $fillable = [
        'price', 'qty', 'description' ,'color', 'size', 'image'
    ];

}

