<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'title', 'brand'
    ];

    public function varient()
    {
        return $this->hasMany(ProductsVarient::class);
    }

    public function subCategory()
    {
        return $this->hasMany(SubCategory::class);
    }
}
