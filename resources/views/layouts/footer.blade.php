<div class="footer fixed-bottom">
        <div class="container footerSection">
            <div class="row py-4">
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Shop</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><a href="#" class="text-muted">Computers</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Phones & tablets</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Gaming & VR</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Audio & Music</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Wearables</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4" >Shop</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><a href="#" class="text-muted">Wearables</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Home entertainment</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">E-mobility</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Special products</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">All categories</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Livlyt</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><a href="#" class="text-muted">About us</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">How it works</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Careers</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Investors</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Livlyt care</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Help</h6>
                    <ul class="list-unstyled mb-0">
                        <li class="mb-2"><a href="#" class="text-muted">+971 5857 05790</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">help@livlyt.com</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">FAQ’s</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Returns policy</a></li>
                        <li class="mb-2"><a href="#" class="text-muted">Contact us</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 mb-lg-0">
                    <h6 class="text-uppercase font-weight-bold mb-4">Be in the know</h6>
                    <p class="text-muted mb-4">Get the latest products, promotions, and design tips in your inbox. </p>
                    <div class="p-1 rounded border">
                        <div class="input-group">
                            <input type="email" placeholder="Enter your email address" aria-describedby="button-addon1" class="form-control border-0 shadow-0">

                        </div>
                    </div>
                    <div class="col-md-12 " style="margin-top:10px">
                        <button class="btn loginBtn">
                            {{ __('Subscribe') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Copyrights -->
        <div class="footerSection" >
            <div class="container text-center flexDiv" >
                <div class="flexSection1">
                    <p class="text-muted mb-0 py-2">© 2022-2022 Livlyt | Privacy  ·  Terms  ·  Sitemap</p>
                </div>
                <div class="flexDiv flexSection2">
                    <p class="text-muted mb-0 py-2">Follow us on</p>
                    <a href="#" class="fa fa-linkedin flexItem"></a>
                    <a href="#" class="fa fa-instagram flexItem"></a>
                    <a href="#" class="fa fa-facebook flexItem"></a>
                </div>
            </div>
        </div>
</div>
