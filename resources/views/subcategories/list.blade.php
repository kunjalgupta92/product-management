@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <a href="{{ url('category') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Category</button></a>
            <a href="{{ url('subcategory') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Sub Category</button></a>
            <a href="{{ url('product') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Products</button></a>
        </div>

      @if(Session::has('message'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      @if(Session::has('error'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ Session::get('error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif

      <div class="container">
          <h3>Add new Subcategory</h3>
        <form action="{{ route('subcategory.store') }}" method="post">
          {{ csrf_field() }}
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="SubCategory Name" aria-label="SubCategory Name" name="subcategoryname" aria-describedby="button-addon2" required>
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Add</button>
            </div>

          </div>
          <div class="input-group mb-3">
            <h6>Select category</h6>
            <select name="categoryname" style="margin-left:50px;width:200px">

              @foreach ($categories as $category)
              <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
              @endforeach
              </select>
            </div>
        </form>

        <table class="table table-hover table-bordered table-stripped">
          <thead>
            <tr>
              <th></th>
              <th>SubCategory Name</th>
              <th>Category Name</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($subcategories as $subcategory)
            <tr>
              <td>{{ $loop->index+1 }}</td>
              <td>{{ $subcategory->name}}</td>
              <td>{{ $subcategory->categoryName}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>

@endsection
