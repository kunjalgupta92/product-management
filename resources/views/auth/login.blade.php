@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="loginSection">
                <div class="card-header">{{ __('Signin') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div>
                            <a class="btn loginBtn btn-block btn-social btn-google" >
                                <span class="fa fa-google"></span> Sign in with Google
                            </a>
                        </div>
                        <div style="margin-top:30px;border-bottom: 1px solid #E0E0E0;"></div>
                        <div class="col-md-12">
                            <label for="email" class="col-md-4 col-form-label">{{ __('Email Address') }}</label>
                        </div>

                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="col-md-12">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>
                        </div>

                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="col-md-12" style="margin-top:10px">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12 " style="margin-top:10px">
                            <button type="submit" class="btn loginBtn">
                                {{ __('Login') }}
                            </button>
                        </div>
                        <div class="col-md-12" style="margin-top:10px">
                            @if (Route::has('password.request'))
                                <a class="forgotPasswordLink" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
