@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <a href="{{ url('category') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Category</button></a>
                    <a href="{{ url('subcategory') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Sub Category</button></a>
                    <a href="{{ url('product') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Products</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
