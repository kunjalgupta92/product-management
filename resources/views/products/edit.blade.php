
   <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  </head>
<body>


    <form action="{{ route('products.update',$products->id) }}" method="post">
          {{ csrf_field() }}
          <div class="input-group mb-3">
            <input type="text" class="form-control" value="{{ $products->title }}" name="productname" aria-describedby="button-addon2">
          </div>
        <div class="input-group mb-3">
            <input type="text" class="form-control" value="{{ $products->brand }}" name="productbrand" aria-describedby="button-addon2">
        </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" value="{{ $products->qty }}" name="productqty" aria-describedby="button-addon2">
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" value="{{ $products->price }}" name="productprice" aria-describedby="button-addon2">
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control" value="{{ $products->description }}" name="productdesc" aria-describedby="button-addon2">
          </div>
          <div class="input-group mb-3">
            <label>Select subcategory</label>
            <select name="subcategoryid" style="margin-left:50px;width:200px">

              @foreach ($subcategories as $subcategory)
              <option value="{{ $subcategory->id }}" selected="selected">{{ $subcategory->name }}</option>
              @endforeach
              </select>
            </div>
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Update</button>
            </div>

          </div>

        </form>


  </body>
</html>
