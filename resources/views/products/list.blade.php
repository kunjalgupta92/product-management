@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <a href="{{ url('category') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Category</button></a>
            <a href="{{ url('subcategory') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Sub Category</button></a>
            <a href="{{ url('product') }}"><button class="btn btn-outline-secondary" type="submit" id="button-addon2">Products</button></a>
        </div>

      @if(Session::has('message'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      @if(Session::has('error'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ Session::get('error') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif

      <div class="container">

    <h3>Add new Products</h3>
          <div class="card-body">
              <form enctype="multipart/form-data" action="{{ route('products.store') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="input-group mb-3">
                      <input type="text" class="form-control" placeholder="Add Product Name" name="productname" aria-describedby="button-addon2" required>
                  </div>
                  <div class="input-group mb-3">
                      <input type="text" class="form-control" placeholder="Add Product Brand" name="productbrand" aria-describedby="button-addon2" required>
                  </div>
                  <div class="input-group mb-3">
                  <label>Select subcategory</label>
                    <select name="subcategoryid" style="margin-left:50px;width:200px">
                    @foreach ($subcategories as $subcategory)
                    <option value="{{ $subcategory->id }}" selected="selected">{{ $subcategory->name }}</option>
                    @endforeach
                    </select>
                  </div>

                  <table class="table table-bordered" id="dynamicAddRemove">
                      <tr>
                          <th>Add Product Varient</th>
                      </tr>
                      <tr>
                          <td><button type="button" name="add" id="add-btn" class="btn btn-success">Add More</button></td>
                      </tr>
                  </table>
                  <button type="submit" class="btn btn-success">Save</button>
              </form>
          </div>
    </div>
    </div>
    <script type="text/javascript">
        $("#add-btn").click(function(){
            $("#dynamicAddRemove").append('<div class=" table-bordered" style="padding:20px">'+
                '<div class="input-group mb-3"><input type="text" class="form-control" placeholder="Add Product Price" name="productprice[]" aria-describedby="button-addon2" required></div>' +
                '<div class="input-group mb-3"><input type="text" class="form-control" placeholder="Add Product Quantity" name="productqty[]" aria-describedby="button-addon2" required></div>' +
                '<div class="input-group mb-3"><input type="text" class="form-control" placeholder="Product Description" name="productdesc[]" aria-describedby="button-addon2" required></div>' +
                '<div class="input-group mb-3"> ' +
                    '<select name="productcolor[]" class="form-control" >' +
                        '<option selected="selected">Select Color</option>' +
                        '<option value="red">Red</option>' +
                        '<option value="blue">Blue</option>' +
                        '<option value="black" >Black</option>' +
                        '<option value="white">White</option>' +
                        '<option value="grey">grey</option>' +
                    '</select></div>' +
                '<div class="input-group mb-3"><input type="text" class="form-control" placeholder="Product Storage/Size" name="productsize[]" aria-describedby="button-addon2" required></div>' +
                '<div class="input-group mb-3">Choose product image:<input type="file" class="form-control" name="productimg[]"  id="image" required></div></div>')
        });
        $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
        });
    </script>

@endsection
