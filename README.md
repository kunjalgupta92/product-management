Install all the dependencies using composer

composer install

Run the database migrations (Set the database connection in .env before migrating)

php artisan migrate

Command will create the encryption keys needed to generate secure access tokens (for API authentication)

php artisan passport:install

Start the local development server

php artisan serve

You can now access the server at http://localhost:8000
